#include <iostream>
using namespace std;
string *arr;
int n;
char getModChar();
void letterRadixSort(int);
int main(){
		//cin >> n;
		n = 1000;
		arr = new string[n];
		for (int i = 0; i < n; i++)
			cin >> arr[i];

		letterRadixSort(3);

		char modChar = getModChar();

		cout << arr[0] << modChar << arr[n-1]<<endl;
		return 0;
	}

	void letterRadixSort( int d) {

		for (int r = d - 1; r >= 0; r--) {
			string *sortedArr = new string[n];
			int k = 26;

			int *c = new int[k + 1];

			for (int i = 0; i < n; i++) {
				c[(int) (arr[i][r]) - 97]++;
			}

			for (int i = 1; i < k + 1; i++)
				c[i] += c[i - 1];

			for (int i = n - 1; i >= 0; i--) {
				sortedArr[c[(int) (arr[i][r]) - 97] - 1] = arr[i];
				c[(int) (arr[i][r]) - 97]--;
			}
			arr = sortedArr;
		}
	}

	char getModChar() {
		char modChar = '\0';
		int *c = new int[26];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < arr[i].length(); j++) {
				c[(int) (arr[i][j]) - 97]++;
			}
		}

		int max = 0;
		for (int i = 0; i < 26; i++) {
			if (max < c[i]) {
				max = c[i];
				modChar = (char) (i + 97);
			}
		}

		return modChar;
}
